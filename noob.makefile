##########################################################################
# Copyright (c) 2009, Romain BARDOU                                      #
# All rights reserved.                                                   #
#                                                                        #
# Redistribution and  use in  source and binary  forms, with  or without #
# modification, are permitted provided that the following conditions are #
# met:                                                                   #
#                                                                        #
# * Redistributions  of  source code  must  retain  the above  copyright #
#   notice, this list of conditions and the following disclaimer.        #
# * Redistributions in  binary form  must reproduce the  above copyright #
#   notice, this list of conditions  and the following disclaimer in the #
#   documentation and/or other materials provided with the distribution. #
# * Neither the  name of Melt nor  the names of its  contributors may be #
#   used  to endorse  or  promote products  derived  from this  software #
#   without specific prior written permission.                           #
#                                                                        #
# THIS SOFTWARE  IS PROVIDED BY  THE COPYRIGHT HOLDERS  AND CONTRIBUTORS #
# "AS  IS" AND  ANY EXPRESS  OR IMPLIED  WARRANTIES, INCLUDING,  BUT NOT #
# LIMITED TO, THE IMPLIED  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR #
# A PARTICULAR PURPOSE  ARE DISCLAIMED. IN NO EVENT  SHALL THE COPYRIGHT #
# OWNER OR CONTRIBUTORS BE  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, #
# SPECIAL,  EXEMPLARY,  OR  CONSEQUENTIAL  DAMAGES (INCLUDING,  BUT  NOT #
# LIMITED TO, PROCUREMENT OF SUBSTITUTE  GOODS OR SERVICES; LOSS OF USE, #
# DATA, OR PROFITS; OR BUSINESS  INTERRUPTION) HOWEVER CAUSED AND ON ANY #
# THEORY OF  LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY,  OR TORT #
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING  IN ANY WAY OUT OF THE USE #
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   #
##########################################################################

include Config

default: all

Config: configure.ml
	ocaml configure.ml

install:
	$(OCAML) install.ml -bin $(INSTALLBIN) -lib $(INSTALLLIB)

uninstall:
	$(OCAML) install.ml -bin $(INSTALLBIN) -lib $(INSTALLLIB) -uninstall

all:
	$(OCAMLC) $(OCAMLINCLUDES) -c -I prelude -I melt -I meltpp -I latex -o prelude/pqueue.cmi prelude/pqueue.mli
	$(OCAMLC) $(OCAMLINCLUDES) -c -I prelude -I melt -I meltpp -I latex -o prelude/clist.cmi prelude/clist.mli
	$(OCAMLC) $(OCAMLINCLUDES) -c -I latex -I melt -I meltpp -I prelude -o latex/variable.cmi latex/variable.mli
	$(OCAMLC) $(OCAMLINCLUDES) -c -I latex -I melt -I meltpp -I prelude -o latex/latex.cmi latex/latex.mli
	$(OCAMLC) $(OCAMLINCLUDES) -c -I prelude -I melt -I meltpp -I latex -o prelude/pqueue.cmo prelude/pqueue.ml
	$(OCAMLC) $(OCAMLINCLUDES) -c -I prelude -I melt -I meltpp -I latex -o prelude/clist.cmo prelude/clist.ml
	$(OCAMLC) $(OCAMLINCLUDES) -c -I latex -I melt -I meltpp -I prelude -o latex/variable.cmo latex/variable.ml
	$(OCAMLC) $(OCAMLINCLUDES) -c -I latex -I melt -I meltpp -I prelude -o latex/latex.cmo latex/latex.ml
	$(OCAMLC) $(OCAMLINCLUDES) -a prelude/pqueue.cmo prelude/clist.cmo latex/variable.cmo latex/latex.cmo -o latex/latex.cma
	cp $(MLPOSTSPECIFIC) melt/mlpost_specific.ml
	$(OCAMLC) $(OCAMLINCLUDES) -c -I melt -I meltpp -I prelude -I latex -o melt/melt_common.cmo melt/melt_common.ml
	$(OCAMLC) $(OCAMLINCLUDES) -c -I melt -I meltpp -I prelude -I latex -o melt/mlpost_specific.cmo melt/mlpost_specific.ml
	$(OCAMLC) $(OCAMLINCLUDES) -c -I melt -I meltpp -I prelude -I latex -o melt/melt.cmi melt/melt.mli
	$(OCAMLC) $(OCAMLINCLUDES) -c -I meltpp -I melt -I prelude -I latex -o meltpp/meltpp_plugin.cmi meltpp/meltpp_plugin.mli
	$(OCAMLC) $(OCAMLINCLUDES) -c -I meltpp -I melt -I prelude -I latex -o meltpp/plugin_private.cmo meltpp/plugin_private.ml
	$(OCAMLC) $(OCAMLINCLUDES) -c -I melt -I meltpp -I prelude -I latex -o melt/melt.cmo melt/melt.ml
	$(OCAMLC) $(OCAMLINCLUDES) -c -I meltpp -I melt -I prelude -I latex -o meltpp/meltpp_plugin.cmo meltpp/meltpp_plugin.ml
	$(OCAMLC) $(OCAMLINCLUDES) -a prelude/pqueue.cmo prelude/clist.cmo latex/variable.cmo latex/latex.cmo melt/melt_common.cmo melt/mlpost_specific.cmo melt/melt.cmo meltpp/plugin_private.cmo meltpp/meltpp_plugin.cmo -o melt/melt.cma
	$(OCAMLC) $(OCAMLINCLUDES) -c -I melt -I meltpp -I prelude -I latex -o melt_version.cmo melt_version.ml
	$(OCAMLC) $(OCAMLINCLUDES) -c -I melt -I meltpp -I prelude -I latex -o totoconf.cmi totoconf.mli
	$(OCAMLC) $(OCAMLINCLUDES) -c -I melt -I meltpp -I prelude -I latex -o melt/tool.cmo melt/tool.ml
	$(OCAMLC) $(OCAMLINCLUDES) -c -I melt -I meltpp -I prelude -I latex -o totoconf.cmo totoconf.ml
	$(OCAMLC) $(OCAMLINCLUDES) unix.cma str.cma bigarray.cma cairo.cma bitstring.cma mlpost.cma latex/variable.cmo prelude/pqueue.cmo prelude/clist.cmo latex/latex.cmo melt/melt_common.cmo melt/mlpost_specific.cmo melt/melt.cmo melt_version.cmo totoconf.cmo melt/tool.cmo -o melt/tool.byte
	$(OCAMLC) $(OCAMLINCLUDES) -c -I latop -I melt -I meltpp -I prelude -I latex -o latop/latop.cmo latop/latop.ml
	$(OCAMLC) $(OCAMLINCLUDES) unix.cma str.cma latop/latop.cmo -o latop/latop.byte
	$(OCAMLLEX) -q meltpp/lexer.mll
	$(OCAMLYACC) meltpp/parser.mly
	$(OCAMLC) $(OCAMLINCLUDES) -c -I meltpp -I melt -I prelude -I latex -o meltpp/ast.cmi meltpp/ast.mli
	$(OCAMLC) $(OCAMLINCLUDES) -c -I meltpp -I melt -I prelude -I latex -o meltpp/parser.cmi meltpp/parser.mli
	$(OCAMLC) $(OCAMLINCLUDES) -c -I meltpp -I melt -I prelude -I latex -o meltpp/lexer.cmo meltpp/lexer.ml
	$(OCAMLC) $(OCAMLINCLUDES) -c -I meltpp -I melt -I prelude -I latex -o meltpp/main.cmo meltpp/main.ml
	$(OCAMLC) $(OCAMLINCLUDES) -c -I meltpp -I melt -I prelude -I latex -o meltpp/parser.cmo meltpp/parser.ml
	$(OCAMLC) $(OCAMLINCLUDES) dynlink.cma unix.cma str.cma bigarray.cma cairo.cma bitstring.cma mlpost.cma latex/variable.cmo prelude/pqueue.cmo prelude/clist.cmo latex/latex.cmo melt/melt_common.cmo melt/mlpost_specific.cmo melt/melt.cmo melt_version.cmo meltpp/parser.cmo meltpp/plugin_private.cmo meltpp/lexer.cmo meltpp/meltpp_plugin.cmo meltpp/main.cmo -o meltpp/main.byte
	$(OCAMLOPT) $(OCAMLINCLUDES) -c -I prelude -I melt -I meltpp -I latex -o prelude/pqueue.cmx prelude/pqueue.ml
	$(OCAMLOPT) $(OCAMLINCLUDES) -c -I prelude -I melt -I meltpp -I latex -o prelude/clist.cmx prelude/clist.ml
	$(OCAMLOPT) $(OCAMLINCLUDES) -c -I latex -I melt -I meltpp -I prelude -o latex/variable.cmx latex/variable.ml
	$(OCAMLOPT) $(OCAMLINCLUDES) -c -I latex -I melt -I meltpp -I prelude -o latex/latex.cmx latex/latex.ml
	$(OCAMLOPT) $(OCAMLINCLUDES) -a prelude/pqueue.cmx prelude/clist.cmx latex/variable.cmx latex/latex.cmx -o latex/latex.cmxa
	$(OCAMLOPT) $(OCAMLINCLUDES) -c -I melt -I meltpp -I prelude -I latex -o melt/melt_common.cmx melt/melt_common.ml
	$(OCAMLOPT) $(OCAMLINCLUDES) -c -I melt -I meltpp -I prelude -I latex -o melt/mlpost_specific.cmx melt/mlpost_specific.ml
	$(OCAMLOPT) $(OCAMLINCLUDES) -c -I meltpp -I melt -I prelude -I latex -o meltpp/plugin_private.cmx meltpp/plugin_private.ml
	$(OCAMLOPT) $(OCAMLINCLUDES) -c -I melt -I meltpp -I prelude -I latex -o melt/melt.cmx melt/melt.ml
	$(OCAMLOPT) $(OCAMLINCLUDES) -c -I meltpp -I melt -I prelude -I latex -o meltpp/meltpp_plugin.cmx meltpp/meltpp_plugin.ml
	$(OCAMLOPT) $(OCAMLINCLUDES) -a prelude/pqueue.cmx prelude/clist.cmx latex/variable.cmx latex/latex.cmx melt/melt_common.cmx melt/mlpost_specific.cmx melt/melt.cmx meltpp/plugin_private.cmx meltpp/meltpp_plugin.cmx -o melt/melt.cmxa
	$(OCAMLOPT) $(OCAMLINCLUDES) -c -I melt -I meltpp -I prelude -I latex -o melt_version.cmx melt_version.ml
	$(OCAMLOPT) $(OCAMLINCLUDES) -c -I melt -I meltpp -I prelude -I latex -o totoconf.cmx totoconf.ml
	$(OCAMLOPT) $(OCAMLINCLUDES) -c -I melt -I meltpp -I prelude -I latex -o melt/tool.cmx melt/tool.ml
	$(OCAMLOPT) $(OCAMLINCLUDES) unix.cmxa str.cmxa bigarray.cmxa cairo.cmxa bitstring.cmxa mlpost.cmxa latex/variable.cmx prelude/pqueue.cmx prelude/clist.cmx latex/latex.cmx melt/melt_common.cmx melt/mlpost_specific.cmx melt/melt.cmx melt_version.cmx totoconf.cmx melt/tool.cmx -o melt/tool.native
	$(OCAMLOPT) $(OCAMLINCLUDES) -c -I latop -I melt -I meltpp -I prelude -I latex -o latop/latop.cmx latop/latop.ml
	$(OCAMLOPT) $(OCAMLINCLUDES) unix.cmxa str.cmxa latop/latop.cmx -o latop/latop.native
	$(OCAMLOPT) $(OCAMLINCLUDES) -c -I meltpp -I melt -I prelude -I latex -o meltpp/parser.cmx meltpp/parser.ml
	$(OCAMLOPT) $(OCAMLINCLUDES) -c -I meltpp -I melt -I prelude -I latex -o meltpp/lexer.cmx meltpp/lexer.ml
	$(OCAMLOPT) $(OCAMLINCLUDES) -c -I meltpp -I melt -I prelude -I latex -o meltpp/main.cmx meltpp/main.ml
	$(OCAMLOPT) $(OCAMLINCLUDES) dynlink.cmxa unix.cmxa str.cmxa bigarray.cmxa cairo.cmxa bitstring.cmxa mlpost.cmxa latex/variable.cmx prelude/pqueue.cmx prelude/clist.cmx latex/latex.cmx melt/melt_common.cmx melt/mlpost_specific.cmx melt/melt.cmx melt_version.cmx meltpp/parser.cmx meltpp/plugin_private.cmx meltpp/lexer.cmx meltpp/meltpp_plugin.cmx meltpp/main.cmx -o meltpp/main.native
	$(OCAMLDOC) $(OCAMLINCLUDES) -dump latex/latex.odoc -hide-warnings -I latex -I melt -I meltpp -I prelude latex/latex.mli
	rm -rf latex/latex.docdir
	mkdir -p latex/latex.docdir
	$(OCAMLDOC) $(OCAMLINCLUDES) -load latex/latex.odoc -html -hide-warnings -d latex/latex.docdir
	$(OCAMLDOC) $(OCAMLINCLUDES) -dump melt/mlpost_specific.odoc -hide-warnings -I melt -I meltpp -I prelude -I latex melt/mlpost_specific.ml
	$(OCAMLDOC) $(OCAMLINCLUDES) -dump melt/melt.odoc -hide-warnings -I melt -I meltpp -I prelude -I latex melt/melt.mli
	rm -rf melt/melt.docdir
	mkdir -p melt/melt.docdir
	$(OCAMLDOC) $(OCAMLINCLUDES) -load melt/mlpost_specific.odoc -load melt/melt.odoc -html -hide-warnings -d melt/melt.docdir
